import axios from 'axios';

export type Range = 'station' | 'region' | 'solarsystem' |'1' | '2' | '3' | '4' | '5' | '10' | '20' | '30' | '40';

export interface MarketOrder {
  order_id: number;
  type_id: number;
  location_id: number;
  volume_total: number;
  volume_remaining: number;
  min_volume: number;
  price: number;
  is_buy_order: boolean;
  duration: number;
  issued: Date;
  range: Range;
}

export interface MarketOrderResponse {
  orders: MarketOrder[];
  expires: Date;
  page: number;
  totalPages: number;
}

export let getPricesForRegion = (regionid: number, page: number): Promise<MarketOrderResponse> => {
  const params = {page: page, datasource: 'tranquility', order_type: 'all'};
  return axios.get(`https://esi.tech.ccp.is/latest/markets/${regionid}/orders/`, {params: params}).then(res => {
    return {
      orders: res.data,
      expires: new Date(res.headers['expires']),
      page: page,
      totalPages: parseInt(res.headers['x-pages']),
    };
  });
};

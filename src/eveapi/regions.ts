import { default as axios, AxiosPromise } from 'axios';

export let getRegions = (): AxiosPromise<number[]> => {
  return axios.get(`https://esi.tech.ccp.is/latest/universe/regions/?datasource=tranquility`);
};

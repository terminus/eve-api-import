import * as market from './eveapi/marketprices';
import * as regions from './eveapi/regions';
import Queue = require('bull');
import * as redis from 'redis';
import * as bluebird from 'bluebird';
import { log } from './util/logging';
import * as fetchProcessors from './processors/fetch';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

process.on('unhandledRejection', (reason: any, p: Promise<any>) => {
  log.error({reason: reason, promise: p}, `Unhandled promise rejection`);
});

interface PromisifiedRedisClient extends redis.RedisClient {
  getAsync(key: string): Promise<string>;
}

const redisUrl = 'redis://192.168.56.112:6379';

// @ts-ignore
const client = redis.createClient(redisUrl) as PromisifiedRedisClient;
const tmp = new Queue('test', redisUrl);
const typeProcessor = new Queue('typeProcessor', redisUrl);

fetchProcessors.register(tmp, typeProcessor);

typeProcessor.process((job, done) => {
  const type = job.data.type;
  const data: market.MarketOrder[] = job.data.orders;
  log.info(`Got ${data.length} orders for type ${type}`);
  done();
  // return client.getAsync(`orders-type-${type}`).then(res => {

  // });
});

log.info('hello');
regions.getRegions().then(res => {
  res.data.forEach((v, i) => tmp.add({region: v}, {delay: (i / 100) * 100 + 1000}));
});


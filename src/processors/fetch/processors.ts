import { log } from '../../util/logging';
import * as market from '../../eveapi/marketprices';
import * as util from '../../util';
import * as _ from 'lodash';
import Queue = require('bull');


export let fetch = () => (job: Queue.Job) => {
  log.info('Start processing of job');
  const region = job.data.region;
  if (!region) {
    return Promise.reject(new Error(`Can't execute job lacking region id`));
  }
  return market.getPricesForRegion(region, 1)
  .then(res => {
    const pagesToFetch = util.range(res.page + 1, res.totalPages + 1);
    const first = Promise.resolve(res);

    return Promise.all(_.concat([first], _.map(pagesToFetch, p => market.getPricesForRegion(10000001, p))))
  });
};

export let mapToSinglePage = (pages: market.MarketOrderResponse[]): market.MarketOrderResponse => {
  const totalPages = (pages && pages.length) ? pages[0].totalPages : 0;
  let flattened = _.flatten(_.map(pages, v => v.orders));
  let expires = _.min(_.map(pages, p => p.expires));
  return {
    page: 1,
    totalPages: totalPages,
    expires: expires!,
    orders: flattened,
  };
};

export let typeSplitter = (orders: market.MarketOrder[], processingFunction: (type: number, orders: market.MarketOrder[]) => void) => {
  let split = _.groupBy(orders, (order: market.MarketOrder) => order.type_id);
  _.forOwn(split, (v, k) => {
    processingFunction(parseInt(k), v);
  });
};

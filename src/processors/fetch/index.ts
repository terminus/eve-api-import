import * as tmp from './processors';
import Queue = require('bull');
import * as moment from 'moment';
import { log } from '../../util/logging';

/**
 * Register processor which will take incoming requests to fetch market orders for a given
 * region and enqueue jobs on the typeProcessingQueue for further processing of found orders.
 * The jobs on the typeProcessingQueue are separated by item type
 * @param queue The queue containing fetch requests
 * @param typeProcessingQueue The queue used to further process fetched market orders
 */
export let register = (queue: Queue.Queue, typeProcessingQueue: Queue.Queue) => {
  let hej = (job: Queue.Job) => tmp.fetch()(job)
  .then(tmp.mapToSinglePage)
  .then(page => {
    let fromNow = moment(page.expires).add(5, 's').diff(moment());
    log.info(`Loaded ${page.orders.length} orders for region ${job.data.region}`);
    log.trace(page);
    log.info(`Queueing another job in ${fromNow} ms`);
    queue.add({region: job.data.region}, {delay: fromNow});
    return page;
  })
  .then(page => tmp.typeSplitter(page.orders, typeProcessingPublisher(typeProcessingQueue)));
  queue.process(hej);
};

let typeProcessingPublisher = (typeProcessingQueue: Queue.Queue) => (typeId: number, orders: any) => {
  typeProcessingQueue.add({type: typeId, orders: orders});
};
